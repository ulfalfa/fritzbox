# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.10...v0.1.0) (2019-12-29)


### ⚠ BREAKING CHANGES

* **serviceId:** since the identifier is now the id and not the type anymore any calls to "exec" are
not working anymore

### Features

* **serviceId:** I switched the service identifier from type to id ([8102209](https://gitlab.com/ulfalfa/fritzbox/commit/810220965ede51cb122056a63570369fa5c33df9))

### [0.0.10](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.9...v0.0.10) (2019-12-26)


### Features

* **fritzbox:** method for getting the current external ipv4 address ([22ff7cf](https://gitlab.com/ulfalfa/fritzbox/commit/22ff7cf56b1f2b07438b543b7319709d58fc7ef0))
* **fritzbox:** method for getting the current external ipv4 address ([6e15e34](https://gitlab.com/ulfalfa/fritzbox/commit/6e15e34f7dea5cefe26279f98fb8dd876092387f))

### [0.0.9](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.8...v0.0.9) (2019-12-21)

### [0.0.8](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.7...v0.0.8) (2019-12-20)


### Features

* **ssl:** auto upgrade to ssl connection ([8b53cee](https://gitlab.com/ulfalfa/fritzbox/commit/8b53cee8b84ad4fbcf068bc73daf54af8637e4ff))


### Bug Fixes

* **eventserver:** checking for mandatory port and address ([8d364c0](https://gitlab.com/ulfalfa/fritzbox/commit/8d364c067b1e4ba204c096ae39dc4b9484b68de3))
* **fritzbox:** reducing parallel calls to fritz to 20 maximum parallel calls ([8b74179](https://gitlab.com/ulfalfa/fritzbox/commit/8b74179a14d1a1ea548b302f731f40b57a2cab8d))

### [0.0.7](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.6...v0.0.7) (2019-12-19)


### Features

* **eventserver:** new eventserver and observe method for fritzbox ([03ade67](https://gitlab.com/ulfalfa/fritzbox/commit/03ade671b9695532cf8b8236c357818967d63c46))

### [0.0.6](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.5...v0.0.6) (2019-12-17)

### [0.0.5](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.4...v0.0.5) (2019-12-17)

### [0.0.4](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.3...v0.0.4) (2019-12-17)

### [0.0.3](https://gitlab.com/ulfalfa/fritzbox/compare/v0.0.2...v0.0.3) (2019-12-17)

### 0.0.2 (2019-12-17)


### Features

* first commit ([b0b81ac](https://gitlab.com/ulfalfa/fritzbox/commit/b0b81acb6eb051feb5c561dfe4cd85d4f5ae8010))
* first working version with 100% test coverage ([5809818](https://gitlab.com/ulfalfa/fritzbox/commit/58098188d93953eaca6c1dbf3e27a4154ec56cf1))
* method for describe the services of Fritz!Box and documentation ([e903bb9](https://gitlab.com/ulfalfa/fritzbox/commit/e903bb93c3cc87ff9e7a5d914566069b3ac8ffad))
